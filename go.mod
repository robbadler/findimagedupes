module gitlab.com/opennota/findimagedupes

require (
	github.com/mattn/go-sqlite3 v1.14.16
	gitlab.com/opennota/magicmime v0.1.2
	gitlab.com/opennota/phash v1.0.4
)

go 1.13
